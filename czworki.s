	section .text
	
	global czworki

czworki:
	push rbp
	mov rbp, rsp

	mov dl, [rdi]
	cmp dl, 0
	jz end 			; string starts with NUL, no operations can be made

	sub rdi, 4

	mov r15d, 0xFF000000
	mov r14d, 0x00FF0000
	mov r13d, 0x0000FF00
	mov r12d, 0x000000FF

prog_loop:

	add rdi, 4

	mov dl, [rdi]
	cmp dl, 0
	jz end 	

	mov eax, [rdi]

	mov r11d, eax
	mov r10d, eax
	mov r9d, eax
	mov r8d, eax

	shl r10d, 8
	shr r9d, 8

	and r11d, r15d
	and r10d, r14d
	and r9d, r13d
	and r8d, r12d

	or r11d, r10d
	or r9d, r8d

	or r11d, r9d

	mov r10d, edi
	and r10d, 0x00000004
	shl r10d, 27
	and r11d, 0xCFFFFFFF
	or r11d, r10d

	mov [rdi], r11d

	jmp prog_loop

end:

	mov rsp, rbp
	pop rbp
	ret					;restoring the stack